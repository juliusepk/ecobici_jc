//
//  ViewController.swift
//  jcecobici
//
//  Created by Julio Serrano on 3/13/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import UIKit

class ViewController: UIViewController, URL_SessionDelegateProtocol {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let requestManager = RequestManager()
        requestManager.delegate = self
        
        let params:NSMutableDictionary = ["client_secret":"2m9bpstw0io0sccogkg40wwg4o0o84kw84swgcsgsg84koc044",
        "client_id":"1670_137419kh4az48kksw0s8gkw44s488ksgs40ww44gwwcw84sw80",
        "grant_type":"client_credentials"]
        requestManager.sendRequestWithData(infoData: params, requestType: .kRequestToken, httpMethod: "POST")
    }
    
    // MARK: - from delegate response
    func responseFromService(response: NSDictionary) {
        print("response: \(response)")
        let method_code:Int = response["method_code"] as! Int
        print("method_code: \(method_code)")
        
        
//        let net_error: String = response["net_error"] as! String
        
//        if net_error == "400" {
//
//        }
        
        if (method_code == RequestType.kRequestToken.rawValue) {
            print("response.response: \(response.object(forKey: "response")!)")
            let dic_response:NSDictionary = response.object(forKey: "response") as! NSDictionary
            let token:String = dic_response["access_token"] as! String
            SessionManager.setToken(token: token)
        }
//        SessionManager.setToken(token: response.)
    }
}

