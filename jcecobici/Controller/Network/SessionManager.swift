//
//  SessionManager.swift
//  jcecobici
//
//  Created by Julius on 3/14/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import Foundation

class SessionManager: NSObject {
    class func setToken(token: String){
        UserDefaults.standard.set(token, forKey: "TOKEN")
    }
    
    class func getToken() -> String {
        let token = UserDefaults.standard.string(forKey: "TOKEN")
        return token ?? ""
    }
}
