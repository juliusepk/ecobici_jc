//
//  RequestManager.swift
//  jcecobici
//
//  Created by Julio Serrano on 3/13/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import Foundation

protocol URL_SessionDelegateProtocol {
    func responseFromService(response: NSDictionary)
}

class RequestManager: NSObject, URLSessionDelegate {
    var delegate: URL_SessionDelegateProtocol?
    
    var dataTask: URLSessionDataTask? //Descarga los bytes, evalua la respuesta del servidor
    var responseData: Data = Data() //Respuesta del lado del servidor
    var httpResponse: HTTPURLResponse?
    
    override init() {
        super.init()
    }
    
    func sendRequestWithData(infoData: NSMutableDictionary, requestType: RequestType, httpMethod: String) {
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        if !Reachability.isConnectedToNetwork(){
            print("no internet")
            delegate?.responseFromService(response: ["net_error": "400", "method_code": requestType])
        } else {
            var request = getBaseRequestToMethod(requestType: requestType, httpMethod: httpMethod)
            let sessionConfig = URLSessionConfiguration.default
            let defaultSession = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: OperationQueue.main)
            responseData = Data()
            
            if httpMethod == "POST" {
                let json_data = try? JSONSerialization.data(withJSONObject: infoData, options: [])
                request.httpBody = json_data
            }

            let postDataTask = defaultSession.dataTask(with: request) { (data, response, error) in
                if error == nil {
                    if let json_serialization = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary {
                        
                        if json_serialization != nil {
                            self.delegate?.responseFromService(response: ["response": json_serialization!, "method_code": requestType.rawValue])
                        } else {
                            self.delegate?.responseFromService(response: ["net_error": "-1002", "method_code": requestType.rawValue])
                            print("Ocurrió un error con la serialización del JSON")
                        }
                    }
                } else {
                    self.delegate?.responseFromService(response: ["net_error": (error! as NSError).code, "method_code": requestType.rawValue])
                }
            }
            postDataTask.resume();
            
        }
        
    }
    
    
    func getBaseRequestToMethod(requestType: RequestType, httpMethod: String) -> URLRequest {
        var request:URLRequest
        let str_url: String = ConfigAmb.getBaseURL() + getURLRequest(requestType: requestType)
        
        request = URLRequest(url: URL.init(string: str_url)!)
        request.httpMethod = httpMethod
        request.timeoutInterval = 60
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        if requestType == RequestType.kRequestStations || requestType == RequestType.kRequestStationStatus {
            let token: String = SessionManager.getToken()
            request.setValue("Bearer "+token, forHTTPHeaderField: "Authorization")
        }
        
        return request
    }
    
    func getURLRequest(requestType:RequestType) -> String{
        switch requestType {
        case .kRequestToken:
            return "oauth/v2/token"
        case .kRequestStations:
            return "api/v1/stations.json"
        case .kRequestStationStatus:
            return "api/v1/stations/status.json"
        }
    }
    
}
