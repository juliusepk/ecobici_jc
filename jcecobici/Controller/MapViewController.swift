//
//  MapViewController.swift
//  jcecobici
//
//  Created by Julio Serrano on 3/15/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, URL_SessionDelegateProtocol {
    func responseFromService(response: NSDictionary) {
        print("response: \(response)")
        
        let method_code:Int = response["method_code"] as! Int
        
        if (method_code == RequestType.kRequestStations.rawValue) {
            print("response.response: \(response.object(forKey: "response")!)")
            let dic_response:NSDictionary = response.object(forKey: "response") as! NSDictionary
            let stations:[NSDictionary] = dic_response.object(forKey: "stations") as! [NSDictionary]
            print("Stations: \(stations)")
            
            for stationData: NSDictionary in stations {
//                var coordinate: CLLocationCoordinate2D
//                let location: NSDictionary = station.object(forKey: "location") as! NSDictionary
//                if
//                    let latitude = location.object(forKey: "lat"),
//                    let longitude = location.object(forKey: "lon"){
//                    coordinate = CLLocationCoordinate2D(latitude: latitude as! CLLocationDegrees, longitude: longitude as! CLLocationDegrees)
//                }else{
//                    coordinate = CLLocationCoordinate2D()
//                }
                print(stationData)
                let station = Station(data: stationData)
                map.addAnnotation(station)
//                map.addAnnotation(Station(title: station.object(forKey: "name") as! String,
//                                        locationName: station.object(forKey: "stationType") as! String,
//                                        coordinate: coordinate))
            }
        }
    }
    

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var selectorMapStyle: UISegmentedControl!
    
    let locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 1000
    var stations: [Station] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkLocationAuthorizationStatus()
        
        
        
        let initialLocation = CLLocation(latitude: 19.503288, longitude: -99.178497)
        
//        let initialLocation = CLLocation(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
        centerMapOnLocation(location: initialLocation)
        
        map.delegate = self
        
        // annotation
//        let station = Station(title: "Test in map",
//                              locationName: "Point to test here",
//                              coordinate: CLLocationCoordinate2D(latitude: 19.665, longitude: (locationManager.location?.coordinate.longitude)!))
//        map.addAnnotation(station)
        
        // get the stations list
        let requestManager = RequestManager()
        requestManager.delegate = self
        requestManager.sendRequestWithData(infoData: [:], requestType: .kRequestStations, httpMethod: "GET")
    }
    
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            map.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        map.setRegion(coordinateRegion, animated: true)
    }
    
    // MARK: - IBActions
    @IBAction func changeStyle(_ sender: Any) {
        switch selectorMapStyle.selectedSegmentIndex {
        case 0:
            map.mapType = .standard
        case 1:
            map.mapType = .satellite
        case 2:
            map.mapType = .hybrid
        default:
            break
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}


extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Station else { return nil }

        let identifier = "marker"
        var view: MKMarkerAnnotationView

        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Station
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
}
