//
//  ConfigAmb.swift
//  jcecobici
//
//  Created by Julius on 3/14/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import Foundation

enum AMBIENTE {
    case kProd
}

class ConfigAmb: NSObject {
    static let ambiente = AMBIENTE.kProd
    static let API_URL_A = "https://pubsbapi.smartbike.com/"
    
    class func getBaseURL() -> String {
        if (ambiente == AMBIENTE.kProd) {
            return API_URL_A
        } else {
            return ""
        }
    }
}
