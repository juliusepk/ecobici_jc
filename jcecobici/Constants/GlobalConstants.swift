//
//  GlobalConstants.swift
//  jcecobici
//
//  Created by Julio Serrano on 3/13/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import Foundation

enum RequestType: Int {
    case kRequestToken, kRequestStations, kRequestStationStatus
}

struct getTokenStruct: Codable {
    let access_token: String
    let expires_in: String
    let token_type: String
    let refresh_token: String
}

struct getStationsStruct: Codable {
    let stations: String
}

struct getStationStatusStruct: Codable {
    let stationStatus:String
}

