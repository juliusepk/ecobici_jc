//
//  TokenData.swift
//  jcecobici
//
//  Created by Julio Serrano on 3/15/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import Foundation

class TokenData: NSObject {
    var access_token: String = ""
    var expires_in: String = ""
    var token_type: String = ""
    var scope: (Any)? = nil
    var refresh_token: String = ""
    
    override init() {
        super.init()
    }
    
    convenience init(dictionary: NSDictionary) {
        self.init()
        
        self.access_token = dictionary["access_token"] as? String ?? ""
        self.expires_in = dictionary["expires_in"] as? String ?? ""
        self.token_type = dictionary["token_type"] as? String ?? ""
        self.scope = dictionary["scope"] as Any
        self.refresh_token = dictionary["refresh_token"] as? String ?? ""
    }
}
