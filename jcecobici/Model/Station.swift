//
//  Station.swift
//  jcecobici
//
//  Created by Julius on 3/15/19.
//  Copyright © 2019 Julio Serrano. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class Station: NSObject, MKAnnotation{
    let title: String?
//    let locationName: String?
//    let coordinate: CLLocationCoordinate2D
    
    
    let adress: String?
    let adressNumber: String?
    let altitude: String?
    let districtCode: String?
    let id: Int?
    //let location: NSDictionary?
//    let name: String?
    let nearbyStations: [Int]?
    let stationType: String?
    let zipCode: Int?
    let coordinate: CLLocationCoordinate2D
    
//    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
//        self.title = title
//        self.locationName = locationName
//        self.coordinate = coordinate
//
//        super.init()
//    }
    
    init(data: NSDictionary){
        var coordinate: CLLocationCoordinate2D
        let location: NSDictionary = data.object(forKey: "location") as! NSDictionary
        if
            let latitude = location.object(forKey: "lat"),
            let longitude = location.object(forKey: "lon"){
            coordinate = CLLocationCoordinate2D(latitude: latitude as! CLLocationDegrees, longitude: longitude as! CLLocationDegrees)
        }else{
            coordinate = CLLocationCoordinate2D()
        }
        self.adress         = data.object(forKey: "adress") as? String ?? "SIN DIRECCIÓN"
        self.adressNumber   = data.object(forKey: "adressNumber") as? String ?? "S/N"
        self.altitude       = data.object(forKey: "altitude") as? String ?? "N/A"
        self.districtCode   = data.object(forKey: "districtCode") as? String ?? "N/A"
        self.id             = data.object(forKey: "id") as? Int ?? -1
//        self.name           = data.object(forKey: "name") as? String ?? "SIN NOMBRE"
        self.title           = data.object(forKey: "name") as? String ?? "SIN NOMBRE"
        self.nearbyStations = data.object(forKey: "nearbyStations") as? [Int] ?? []
        self.stationType    = data.object(forKey: "stationType") as? String ?? "SIN TIPO"
        self.zipCode        = data.object(forKey: "zipCode") as? Int ?? -1
        self.coordinate     = coordinate
        
        super.init()
    }
    
//    init(json: [Any]){
//        self.title = title
//        self.locationName = locationName
//        self.coordinate = coordinate
//    }
    
    var subtitle: String? {
        return stationType
    }
    
    // open the map
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
